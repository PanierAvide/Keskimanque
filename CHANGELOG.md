# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Next version (to be released)

### Changed
- Minimum zoom to see features in detail is now 14

### Fixed
- Map coordinates in URL not lost anymore after theme change


## 0.2.2 - 2020-01-12

### Added
- Categories have now statistics for zoom levels < 13 (average of all themes)
- Details zoom levels (>= 13) shows all features for categories
- Support of alternative conditions for themes tags
- Themes for tourism and public services

### Changed
- Reduced city colors to 5 categories (no data + very few data grouped)
- Error messages translated in French
- Features points for details zoom levels are now blue
- Analysis script for DB better handles updates


## 0.2.1 - 2020-01-10

### Added
- Last data update date is shown in UI in "About" popup
- Click on sea hides bottom menu
- Legend for themes and categories in "About" popup

### Changed
- Better style for themes in city information popup
- Addresses theme uses API features at details zoom levels instead of BANO tiles
- Better layout on mobile
- Color legend is now hidden on mobile (moved to "About" popup)


## 0.2.0 - 2020-01-09

### Added
- Categories for grouping themes
- Map position and theme are stored in URL
- Click anywhere on map shows city statistics
- When zoomed over a city, tiles or features can be retrieved for a given theme

### Changed
- Theme selector in UI is now on top-right corner with icons
- Compatibility with a broader range of web browsers
- City information popup replaced by a responsive widget
- Missing feature index based on both relative and absolute feature density


## 0.1.0 - 2019-12-29

### Added
- Initial release (configurable backend, basic UI)
