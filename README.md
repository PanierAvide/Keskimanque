# Keskimanque

Keskimanque est une carte permettant de trouver facilement les communes où des données OpenStreetMap sont probablement manquantes. L'objectif est de faciliter la contribution pour les bénévoles en ciblant des catégories d'objets précises par commune.

L'outil est disponible à cette adresse : [keskimanque.pavie.info](https://keskimanque.pavie.info)

### Méthodologie

Le calcul se base sur deux éléments : la population de la commune, et la densité d'objets recherchés. On calcule une moyenne de densité pour chaque catégorie de communes (moins de 500 habitants, moins de 1000...). Puis, on compare la densité pour une commune précise à la moyenne. Si la ville est sous la moyenne, elle apparaît en rouge. Si elle est au-dessus, elle apparaît en vert.

Bien sûr, cette méthode induit des biais : si une commune ne dispose pas de toilettes publiques sur le terrain, elle sera en rouge même si la carte est techniquement complète. De même, si une catégorie d'objet est peu représenté dans OpenStreetMap, une commune dont il manque la moitié des objets peut apparaître en vert car elle est plus renseignée que la moyenne. Malgré tout, cela donne une indication intéressante sur tout le territoire, ce qui ne serait pas forcément le cas avec d'autres méthodes.


## Installation

Les dépendances sont les suivantes :

* NodeJS >= 9
* PostgreSQL >= 9.6 et ses extensions PostGIS, HStore
* wget
* [osm2pgsql](https://github.com/openstreetmap/osm2pgsql)
* [jq](https://stedolan.github.io/jq/)
* GDAL >= 2.1 (et en particulier la commande `ogr2ogr`)
* tar et xz
* [osmium-tool](https://github.com/osmcode/osmium-tool) >= 1.11

Installation des dépendances sous Debian 9 :

```bash
sudo apt-get install wget jq gdal-bin
cd ~

# Node JS 12
wget https://deb.nodesource.com/setup_12.x -O nodejs.sh
sudo bash nodejs.sh
sudo apt-get install nodejs
rm nodejs.sh

# Osm2PgSQL latest
sudo apt-get install git make cmake g++ libboost-dev libboost-system-dev libboost-filesystem-dev libexpat1-dev zlib1g-dev libbz2-dev libpq-dev libproj-dev lua5.2 liblua5.2-dev
git clone git://github.com/openstreetmap/osm2pgsql.git
cd osm2pgsql
mkdir build && cd build
cmake ..
make
sudo make install
cd ../../
rm -rf osm2pgsql

# Osmium-tool
sudo apt-get install libboost-program-options-dev libbz2-dev zlib1g-dev libexpat1-dev
mkdir work
cd work
git clone https://github.com/mapbox/protozero
cd protozero
mkdir build
cd build
cmake ..
make
sudo make install
cd ../../
git clone https://github.com/osmcode/libosmium
cd libosmium
mkdir build
cd build
cmake ..
make
sudo make install
cd ../../
git clone https://github.com/osmcode/osmium-tool
cd osmium-tool
mkdir build
cd build
cmake ..
make
sudo make install
cd ../../

# PostgreSQL 11
sudo echo "deb http://apt.postgresql.org/pub/repos/apt/ stretch-pgdg main" > /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install postgresql-11 postgresql-client-11 postgresql-11-postgis-3
```

Téléchargement du code de Keskimanque :

```bash
git clone https://framagit.org/PanierAvide/Keskimanque.git
cd Keskimanque
```


## Configuration

### Paramètres généraux

Les paramètres généraux de configuration du service sont à renseigner dans le fichier `config.json`. Les variables à renseigner sont les suivantes :

* `WORK_DIR` : dossier de travail, dans lequel les fichiers temporaires sont stockés (exemple: `/tmp/keskimanque`)
* `PBF_URL` : URL de téléchargement de l'extrait OpenStreetMap au format PBF (exemple: `http://download.openstreetmap.fr/extracts/europe/france-latest.osm.pbf`)
* `PG_HOST` : nom d'hôte pour accéder à la base de données PostgreSQL (exemple: `localhost`)
* `PG_USER` : nom d'utilisateur pour accéder à la base de données PostgreSQL (exemple: `postgres`)
* `PG_USER` : numéro de port pour accéder à la base de données PostgreSQL (exemple: `5432`)
* `PG_DB_CLEAN` : nom de la base de données qui contiendra les données propres (exemple: `keskimanque`)
* `PG_DB_TMP` : nom de la base de données qui contiendra les données en cours d'import (exemple: `keskimanque_next`)
* `PG_DB_OLD` : nom de la base de données qui contiendra les données précédemment en production (exemple: `keskimanque_prev`)
* `PARALLEL_JOBS` : nombre de tâches à exécuter en parallèle, idéalement 75% des coeurs du processeur (exemple: `6`)
* `CACHE_MEM` : mémoire à allouer lors de l'import de données, en MB, idéalement 75% de la mémoire vive disponible (exemple: `10000`)
* `FLAT_NODES` : activer l'option `--flat-nodes` de l'outil osm2pgsql, utile pour les imports de grosses régions ou de pays (`true` pour activer, `false` pour désactiver)

### Thèmes

Les thèmes de données sont à configurer dans le dossier `themes/`. Les thèmes sont regroupés par catégories. Les catégories sont décrites dans le fichier `themes/categories.json`. À chaque catégorie est associée un dossier du même nom, ainsi qu'une icône au format SVG.

Chaque thème est représenté par un fichier `.json` avec une structure précise, exemple :

```json
{
  "theme_name": "Adresses",
  "theme_geomtypes": [ "point", "polygon" ],
  "theme_where": { "addr:housenumber": "*" },
  "theme_details": { "tms": { "url": "...", "attribution": "Carto" } }
}
```

Les propriétés sont les suivantes :

* `theme_name` : nom du thème pour affichage à l'utilisateur
* `theme_geomtypes` : géométries pertinentes pour l'analyse (tableau dont les valeurs sont `point`, `line`, `polygon`)
* `theme_where` : tags à utiliser pour isoler les objets (exemple : `"amenity": "bench"`). La valeur peut être `*` pour lire toutes les valeurs d'une clé particulière. Le séparateur `|` peut être utilisé pour lister plusieurs valeurs (exemple : `"leisure": "track|pitch"`)
* `theme_details` : source de données pour les zooms de détails. Au choix `{ "api": "true" }` pour interroger l'API, ou `{ "tms": { "url": "https://..." } }` pour afficher un fond de carte TMS


## Base de données

La base de données contient les informations issues d'OpenStreetMap, afin de les mettre à disposition de l'API.

### Script d'import

La configuration de la structure des données en bases (tags sous forme de colonnes, type d'objets sous forme d'emprise...) sont à détailler dans le fichier `db/import.style`. Un exemple complet de ce type de fichier [est disponible ici](https://github.com/openstreetmap/osm2pgsql/blob/master/default.style). Il est recommandé de ne faire apparaître sous forme de colonne dédiée uniquement les tags qui feront partie des filtres des requêtes formulées par l'API.

Une fois les configurations réalisées, exécutez simplement les commandes suivantes :

```bash
cd db/
./0_update_db.sh # Pour mettre à jour la base en elle-même
./1_analysis.sh next # Pour lancer les analyses de données
```


## API

L'API met à disposition le site web client, ainsi que les données selon la zone chargée par l'utilisateur. Elle interroge la base de données PostgreSQL.

### Installation des dépendances

Pour s'exécuter, l'API nécessite que NodeJS (>= 9) soit installé sur la machine. Pour installer les dépendances, lancez les commandes suivantes :

```bash
cd api/
npm install
```

### Exécution

Pour lancer l'API, il suffit d'exécuter la commande suivante :

```bash
npm run start

# Alternativement, vous pouvez changer le numéro de port avec cette commande
PORT=12345 npm run start
```

### Utilisation

Une fois l'API lancée, le site web devient accessible à l'adresse [localhost:3000](http://localhost:3000/).


## Licence

Copyright (c) Adrien Pavie 2019

Mis à disposition sous licence AGPL, voir [LICENSE](LICENSE.txt) pour le texte complet de la licence.
