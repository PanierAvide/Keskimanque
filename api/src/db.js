/**
 * This file manages communication with PostgreSQL database
 */

const { Pool } = require('pg');
const fs = require('fs');
const themeCategories = require('./themes');
const CONFIG = require('../../config.json');

// Conversion between diff var names in DB and category ID
const diffVarToCategory = {};
Object.entries(themeCategories)
.filter(e => e[1].themes !== null)
.forEach(e => Object.keys(e[1].themes)
	.forEach(k => {
		diffVarToCategory["diff_"+k] = e[0];
		diffVarToCategory["avg_"+k] = e[0];
	})
);

// Array flattening
const flatten = function(arr, result = []) {
	for (let i = 0, length = arr.length; i < length; i++) {
		const value = arr[i];
		if (Array.isArray(value)) {
			flatten(value, result);
		} else {
			result.push(value);
		}
	}
	return result;
};

// Create pool of connections
let pool = null;

function createPool() {
	pool = new Pool({
		user: CONFIG.PG_USER,
		host: CONFIG.PG_HOST,
		database: CONFIG.PG_DB_CLEAN,
		port: CONFIG.PG_PORT
	});
}

/**
 * Checks if database is ready to receive requests (running and not being updated)
 */
function whenAvailable(t) {
	t = t || 0;

	if(t === 10) {
		pool = null;
		return Promise.reject(new Error("Database is not available"));
	}

	return new Promise((resolve, reject) => {
		if(fs.existsSync(CONFIG.WORK_DIR+"/db.lock")) {
			setTimeout(() => {
				resolve(whenAvailable(t+1));
			}, 1000);
		}
		else {
			if(pool === null) {
				createPool();
			}

			pool.query("SELECT 1+1")
				.then(() => resolve())
				.catch(() => {
					setTimeout(() => {
						resolve(whenAvailable(t+1));
					}, 1000);
				});
		}
	});
}

/**
 * Get information about city at given coordinates
 * @param {number} lat Latitude (WGS84)
 * @param {number} lng Longitude (WGS84)
 * @return {Promise} Resolves on metadata <pre>{ "name": "Laville", "population": 12345, "missing": [ "bus_stop", "defibrillator" ] }</pre>
 */
function whereAmI(lat, lng) {
	const sql = `
WITH c AS (SELECT ST_Transform(ST_SetSRID(ST_Point($1, $2), 4326), 3857) AS geom)
SELECT zd.*
FROM zones z
JOIN c ON z.geom && c.geom AND ST_Contains(z.geom, c.geom)
JOIN zones_data zd ON z.gid = zd.gid
LIMIT 1`

	if(pool === null) {
		createPool();
	}

	return pool.query(sql, [ lng, lat ])
	.then(result => {
		if(result.rows.length !== 1) {
			return {};
		}
		else {
			const d = result.rows[0];
			const json = { name: d.name, population: d.population, missing: [] };

			Object.entries(d)
			.filter(e => e[0].startsWith("diff_"))
			.forEach(e => {
				const [ k, v ] = e;
				if(v < 0.5) {
					json.missing.push({ v: parseFloat(v), k: k.substring(5) });
				}
			});

			json.missing = json.missing
				.sort((a,b) => a.v - b.v)
				.map(kv => kv.k);

			return json;
		}
	});
}

/**
 * Extract data in bounding box
 * @param {Object} bbox The coordinates
 * @return {Promise} Resolves on GeoJSON feature collection
 */
function getData(bbox) {
	return whenAvailable()
	.then(() => {
		const bboxTxt = `POLYGON((${bbox.minlng} ${bbox.minlat}, ${bbox.maxlng} ${bbox.minlat}, ${bbox.maxlng} ${bbox.maxlat}, ${bbox.minlng} ${bbox.maxlat}, ${bbox.minlng} ${bbox.minlat}))`;
		return pool.query("SELECT *, ST_AsGeoJSON(ST_Transform(geom, 4326))::json AS geojson FROM zones_data WHERE ST_Contains(ST_Transform(ST_SetSRID(ST_GeomFromText($1), 4326), 3857), geom)", [ bboxTxt ])
		.then(result => {
			const geojson = { type: "FeatureCollection", features: [], statistics: {} };

			geojson.features = result.rows
			.map(r => {
				const statsCat = { all: { sum: 0, count: 0 } };
				Object.keys(themeCategories).forEach(k => statsCat[k] = { sum: 0, count: 0 });

				const f = { type: "Feature", properties: {}, geometry: r.geojson };
				let sumAllDiff = 0;
				let countDiff = 0;
				Object.entries(r).forEach(e => {
					const [ k, v ] = e;
					if(k === "gid") { f.id = v; }
					else if(!["geojson", "geom"].includes(k)) { f.properties[k] = v; }

					if(k.startsWith("diff_")) {
						// Add to theme cat
						statsCat[diffVarToCategory[k]].sum += v;
						statsCat[diffVarToCategory[k]].count++;

						// Add to global stat
						statsCat.all.sum += v;
						statsCat.all.count++;
					}
				});

				Object.entries(statsCat).forEach(e => {
					f.properties["diff_"+e[0]] = e[1].sum / e[1].count;
				});

				return f;
			});

			// Retrieve statistics
			return pool.query("SELECT * FROM zones_stats")
			.then(stats => {
				geojson.statistics = {};
				stats.rows.forEach(r => {
					const avgStatsCat = { all: { sum: 0, count: 0 } };
					Object.keys(themeCategories).forEach(k => avgStatsCat[k] = { sum: 0, count: 0 });

					geojson.statistics[r.cat_pop] = {};
					Object.entries(r).filter(e => e[0].startsWith("avg_")).forEach(e => {
						geojson.statistics[r.cat_pop][e[0]] = e[1];
						avgStatsCat[diffVarToCategory[e[0]]].sum += e[1];
						avgStatsCat[diffVarToCategory[e[0]]].count++;
						avgStatsCat.all.sum += e[1];
						avgStatsCat.all.count++;
					});

					Object.entries(avgStatsCat).forEach(e => {
						geojson.statistics[r.cat_pop]["avg_"+e[0]] = e[1].sum / e[1].count;
					});
				});
				return geojson;
			});
		});
	});
}

/**
 * Extract data in bounding box for a given theme
 * @param {Object} bbox The coordinates
 * @param {Object} theme The theme metadata
 * @return {Promise} Resolves on GeoJSON feature collection
 */
function getDataForTheme(bbox, theme) {
	return whenAvailable()
	.then(() => {
		const bboxTxt = `POLYGON((${bbox.minlng} ${bbox.minlat}, ${bbox.maxlng} ${bbox.minlat}, ${bbox.maxlng} ${bbox.maxlat}, ${bbox.minlng} ${bbox.maxlat}, ${bbox.minlng} ${bbox.minlat}))`;

		let sql = `
WITH bbox AS (
	SELECT ST_Transform(ST_SetSRID(ST_GeomFromText($1), 4326), 3857) AS geom
)`;
		const themeCond = (Array.isArray(theme.theme_where) ? theme.theme_where : [theme.theme_where])
			.map(tc => "("+Object.entries(tc)
				.map(e => {
					if(e[1] === '*') { return `p."${e[0]}" IS NOT NULL`; }
					else if(e[1].includes("|")) { return `p."${e[0]}" IN ('${e[1].split("|").join("', '")}')`; }
					else { return `p."${e[0]}" = '${e[1]}'`; }
				})
				.join(" AND ")+")"
			)
			.join(" OR ");

		theme.theme_geomtypes.forEach((g, i) => {
			if(i > 0) { sql += ' UNION '; }
			sql += `
SELECT ST_AsGeoJSON(ST_Transform(${g === "point" ? "p.way" : "ST_Centroid(p.way)"}, 4326)) AS geojson
FROM planet_osm_${g} p
JOIN bbox ON ${themeCond} AND bbox.geom && p.way AND ST_Intersects(bbox.geom, p.way)`;
		});

		return pool.query(sql, [ bboxTxt ])
		.then(result => {
			const geojson = { type: "GeometryCollection" };
			geojson.geometries = result.rows.map(r => JSON.parse(r.geojson));
			return geojson;
		});
	});
}

/**
 * Extract data in bounding box for a given category
 * @param {Object} bbox The coordinates
 * @param {Object} category The category metadata
 * @return {Promise} Resolves on GeoJSON feature collection
 */
function getDataForCategory(bbox, category) {
	const promises = Object.values(category.themes).map(t => getDataForTheme(bbox, t));
	return Promise.all(promises)
	.then(results => {
		return {
			type: "GeometryCollection",
			geometries: flatten(results.map(r => r.geometries))
		};
	});
}

module.exports = {
	getData: getData,
	getDataForTheme: getDataForTheme,
	getDataForCategory: getDataForCategory,
	whereAmI: whereAmI
};
