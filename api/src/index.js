/**
 * API main code
 */

const CONFIG = require('../../config.json');
const express = require('express');
const cors = require('cors');
const compression = require("compression");
const fs = require('fs');
const db = require('./db');
const themes = require('./themes');
const themesById = Object.assign({}, ...Object.values(themes).map(c => c.themes));

// Init API
const app = express();
const port = process.env.PORT || 3000;
app.use(cors());
app.options('*', cors());
app.use(compression());


/*
 * List of routes
 */

app.use('/', express.static(__dirname+'/../build'));

app.get('/status', (req, res) => {
	const lastTsFile = CONFIG.WORK_DIR+"/last_ts.txt";
	fs.readFile(lastTsFile, (err, lastTs) => {
		if(err) {
			return res.status(500).send("Error during processing: "+err.message);
		}
		res.send({
			data_timestamp: parseInt(lastTs.toString())
		});
	});
});

app.get("/themes", (req, res) => {
	res.send({ categories: themes });
});

app.get('/data/stats', (req, res) => {
	try {
		// Check bbox parameter
		if(!req.query.bbox || !/^(-?\d+(\.\d+)?)(,(-?\d+(\.\d+)?)){3}$/.test(req.query.bbox)) {
			throw new Error("bbox parameter should be a list of 4 coordinates : lng1,lat1,lng2,lat2");
		}

		const [ lng1, lat1, lng2, lat2 ] = req.query.bbox.split(",").map(c => parseFloat(c));
		const bbox = { minlng: lng1, minlat: lat1, maxlng: lng2, maxlat: lat2 };

		db.getData(bbox)
		.then(geojson => res.send(geojson))
		.catch(e => res.status(500).send("Error during processing: "+e.message));
	}
	catch(e) {
		res.status(400).send("Invalid request "+e.message);
	}
});

app.get('/data/pois/:theme', (req, res) => {
	try {
		// Check bbox parameter
		if(!req.query.bbox || !/^(-?\d+(\.\d+)?)(,(-?\d+(\.\d+)?)){3}$/.test(req.query.bbox)) {
			throw new Error("bbox parameter should be a list of 4 coordinates : lng1,lat1,lng2,lat2");
		}

		// Check theme parameter
		if(!themesById[req.params.theme] && !themes[req.params.theme]) {
			throw new Error("unknown theme: "+req.params.theme);
		}
		else if(req.params.theme === "all") { return { type: "GeometryCollection", geometries: [] }; }

		const [ lng1, lat1, lng2, lat2 ] = req.query.bbox.split(",").map(c => parseFloat(c));
		const bbox = { minlng: lng1, minlat: lat1, maxlng: lng2, maxlat: lat2 };
		const fct = themesById[req.params.theme] ? db.getDataForTheme : db.getDataForCategory;
		fct(bbox, themesById[req.params.theme] || themes[req.params.theme])
		.then(geojson => res.send(geojson))
		.catch(e => res.status(500).send("Error during processing: "+e.message));
	}
	catch(e) {
		res.status(400).send("Invalid request "+e.message);
	}
});

app.get('/whereami', (req, res) => {
	try {
		const lat = parseFloat(req.query.lat);
		const lng = parseFloat(req.query.lng);

		if(isNaN(lat) || isNaN(lng)) {
			throw new Error("Invalid coordinates, must set lat and lng parameters as decimal numbers");
		}

		db.whereAmI(lat, lng)
		.then(json => res.send(json))
		.catch(e => res.status(500).send("Error during processing: "+e.message));
	}
	catch(e) {
		res.status(400).send("Invalid request "+e.message);
	}
});

// 404
app.use((req, res) => {
	res.status(404).send(req.originalUrl + ' not found')
});


// Start
app.listen(port, () => {
	console.log('API started on port: ' + port);
});
