/**
 * Allows reading SQL files containing themes metadata
 */

const fs = require('fs');
const folder = '../themes';
const categories = JSON.parse(fs.readFileSync(folder+'/categories.json'));
const jsonrgx = /^[A-Za-z0-9_\-]+\.json$/;

Object.keys(categories).forEach(cid => {
	categories[cid].themes = {};

	fs.readdirSync(folder + "/" + cid).forEach((file) => {
		if(jsonrgx.test(file)) {
			try {
				const themeId = file.substring(0, file.length - 5);
				const theme = JSON.parse(fs.readFileSync(`${folder}/${cid}/${file}`, 'utf8'));
				categories[cid].themes[themeId] = theme;
			}
			catch(e) {
				throw new Error("Invalid theme file: "+file+" "+e.message);
			}
		}
		else {
// 			console.log("Ignored file: "+file);
		}
	});
});

categories.all = { name: "Tous", themes: null };

module.exports = categories;
