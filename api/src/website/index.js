const utils = require('./utils');

// Some constants
const MARKER_SIZES = { "mt0": 4, "mt500": 6, "mt1000": 8 , "mt5000": 10, "mt10000": 12, "mt50000": 13, "mt100000": 14, "mt250000": 15 };
const LEGEND = { /*neg3: { label: "Aucun élément", color: "#96281b", order: 0 },*/ neg2: { label: "Très peu d'éléments", color: "#96281b", order: 1 }, neg1: { label: "Peu d'éléments", color: "#f03434", order: 2 }, avg: { label: "Dans la moyenne", color: "#fabe58", order: 3 }, pos1: { label: "Au-dessus de la moyenne", color: "#87d37c", order: 4 }, pos2: { label: "Parmi les plus complets", color: "#1e824c", order: 5 } };
const STYLE_FEATURE_POINTS = { stroke: false, fillColor: "#1f3a93", fillOpacity: 0.8, radius: 5, interactive: false };
const ZOOM_DETAILS = 14;
const ZOOM_CITIES = 9;

// Global variables
const data = {};
const url = window.location.origin + window.location.pathname.substring(0, window.location.pathname.lastIndexOf('/')) + "/";
let lastCloseBottomMenuClick = null;
let timerDl = null;
let lastBounds = null;
let isDl = false;
let geojsonLayer = null;
let featuresLayer = null;
let lastType = null;
let currentTheme = null;
let currentCategory = null;

// Main DOM elements
const domDock = document.getElementById("dock");
const domDockLoader = document.getElementById("dock-loader");
const domDockZoomin = document.getElementById("dock-zoomin");
const domAbout = document.getElementById("about");
const domIconDockCat = document.getElementById("icon-dock-cat");
const domIconDockThemes = document.getElementById("icon-dock-theme");
const domBottomMenu = document.createElement("div");
const domBottomMenuContent = document.createElement("div");
const domLegend = document.createElement("div");
const domTsData = document.getElementById("ts-data");
const domLegendModalMap = document.getElementById("legend-modal-map");
const domLegendModalThemes = document.getElementById("legend-modal-themes");

/*
 * Data download from API
 */

utils.request(url+"themes")
	.then(themes => {
		data.themesCategories = JSON.parse(themes).categories;
		data.themes = Object.assign({}, ...Object.values(data.themesCategories)
			.map(c => c.themes)
			.filter(t => t !== null)
		);

		Object.entries(data.themesCategories)
		.filter(e => e[1].themes !== null)
		.forEach(e => Object.entries(e[1].themes)
			.forEach(e2 => e2[1].category = e[0])
		);

		// Retrieve theme/category to display
		let themecatToLoad = utils.getUrlParameter(window.location.href, "t");
		if(data.themesCategories[themecatToLoad]) {
			currentCategory = themecatToLoad;
			currentTheme = null;
		}
		else if(data.themes[themecatToLoad]) {
			currentTheme = themecatToLoad;
			currentCategory = data.themes[themecatToLoad].category;
		}
		else {
			currentCategory = "all";
			currentTheme = null;
		}

		// Display categories/themes
		showCategories();
		changeTheme(currentTheme);
		afterZoom();
	})
	.catch(e => {
		data.themesCategories = "error";
		console.error(e);
		alert("Erreur lors du téléchargement des thèmes. Merci de réessayer plus tard.");
	});

utils.request(url+"status")
	.then(data => JSON.parse(data))
	.then(data => {
		data.status = data;
		domTsData.classList.remove("spinner-grow", "spinner-grow-sm", "text-primary");
		domTsData.classList.add("d-inline");
		domTsData.innerHTML = (new Date(data.data_timestamp * 1000)).toISOString().split("T")[0];
	})
	.catch(e => {
		data.status = "error";
		console.error(e);
	});


/*
 * Map init
 */

const map = L.map("map", {
	preferCanvas: true,
	maxBounds: [[47.257,-5.219], [48.908,0]],
	minZoom: 7
})
.setView([48.025,-2.895], ZOOM_CITIES);

map.attributionControl.setPrefix("&copy; <a href='https://pavie.info/'>Adrien PAVIE</a> 2019");
map.addHash();

const lloc = L.control.locate({
	setView: "always",
	onLocationError: e => console.error(e),
	locateOptions: { maxZoom: 17, enableHighAccuracy: true }
}).addTo(map);
setTimeout(() => lloc.start(), 500);
let tmsLayer = null;

const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
})
.addTo(map);


/*
 * Map widgets
 */

// Bottom menu (city metadata)
const leafletBottomMenu = L.control({ position: "bottomright" });
leafletBottomMenu.onAdd = map => {
	domBottomMenu.classList.add("km-bottom-menu");
	domBottomMenu.classList.add("d-none");
	domBottomMenu.addEventListener("click", e => e.stopPropagation());

	// Close button
	const close = document.createElement("span");
	close.innerHTML = '❌';
	close.classList.add("km-bottom-menu-close");
	close.addEventListener("click", e => {
		lastCloseBottomMenuClick = Date.now();
		domBottomMenu.classList.add("d-none");
	});
	domBottomMenu.appendChild(close);

	domBottomMenu.appendChild(domBottomMenuContent);

	return domBottomMenu;
};
leafletBottomMenu.addTo(map);

function showCurrentCity(p) {
	if(!p.name) {
		domBottomMenu.classList.add("d-none");
		return null;
	}

	const missing = p.missing
		.map(tid => Object.assign({}, { id: tid }, data.themes[tid]))
		.slice(0, 4)
		.map(t => {
			const domImg = document.createElement("img");
			domImg.src = `img/${t.id}.svg`
			domImg.title = t.theme_name;
			domImg.alt = t.theme_name;
			domImg.classList.add("km-missing-theme");
			domImg.addEventListener("click", () => changeTheme(t.id));
			return domImg;
		});

	const html = `<h3>${p.name}</h3><p class="m-0"><b>Population</b> : ${p.population}<br /><b>Contributions à prioriser</b> :</p>`;
	domBottomMenuContent.innerHTML = html;
	const domImgs = document.createElement("p");
	domImgs.classList.add("m-0");
	domImgs.classList.add("mt-1");
	domImgs.classList.add("text-center");
	domImgs.classList.add("km-missing-themes-imgs");

	if(missing.length === 0) {
		domImgs.innerHTML = "Aucun";
	}
	else {
		missing.forEach(m => domImgs.appendChild(m));
	}

	domBottomMenuContent.appendChild(domImgs);
	domBottomMenu.classList.remove("d-none");
}

// Color legend
const leafletLegend = L.control({ position: "bottomleft" });
leafletLegend.onAdd = map => {
	domLegend.classList.add("km-legend");
	domLegend.addEventListener("click", e => e.stopPropagation());

	const entries = Object.values(LEGEND).sort((a,b) => a.order - b.order);
	entries.forEach(e => {
		const html = `<span style="color: ${e.color}; font-size: 2em">●</span> ${e.label}`;
		// Legend for desktop
		const edom = L.DomUtil.create('span', 'km-legend-entry km-r-desktop');
		edom.innerHTML = html;
		domLegend.appendChild(edom);

		// Legend for about modal
		const edom2 = L.DomUtil.create('span', 'km-legend-entry');
		edom2.innerHTML = html;
		domLegendModalMap.appendChild(edom2);
	});

	// Button for mobile
	const dombtn = L.DomUtil.create('a', 'km-legend-info km-r-mobile');
	dombtn.innerHTML = '<i class="fa fa-info text-center" style="width: 36px"></i>';
	dombtn.href = '#';
	dombtn.title = 'À propos';
	dombtn.addEventListener("click", e => {
		e.stopPropagation();
		setTimeout(() => $("#aboutm").modal('show'), 500);
	});
	domLegend.appendChild(dombtn);

	return domLegend;
};
leafletLegend.addTo(map);

// Event handler for category click on switcher
function onCategoryClick(e) {
	const target = e.target.nodeName.toLowerCase() === "img" ? e.target.parentNode : e.target;
	changeCategory(target.getAttribute("data-category"));
	changeTheme(null);
}

// Categories/themes switcher
function showCategories() {
	domIconDockCat.innerHTML = '';
	domIconDockThemes.innerHTML = '';
	domLegendModalThemes.innerHTML = '';

	const domBrand = document.createElement("span");
	domBrand.innerHTML = "KM";
	domBrand.classList.add("icon-dock-brand", "km-r-mobile");
	domIconDockCat.appendChild(domBrand);

	Object.entries(data.themesCategories)
	.sort((a,b) => a[0].localeCompare(b[0]))
	.forEach((e, i) => {
		const [ cid, category ] = e;

		// Categories for switcher
		const domCategory = document.createElement("a");
		domCategory.setAttribute("data-category", cid);
		domCategory.setAttribute("href", "#");
		domCategory.title = category.name;

		const domCatImg = document.createElement("img");
		domCatImg.src = `img/${cid}.svg`;
		domCatImg.title = category.name;
		domCategory.appendChild(domCatImg);

		if(cid === currentCategory) {
			domCategory.classList.add("icon-dock-active");
			showThemes(cid);
		}

		domCategory.addEventListener("click", onCategoryClick);
		domIconDockCat.appendChild(domCategory);

		// Categories for legend
		const domCategoryFirst = document.createElement("div");
		domCategoryFirst.innerHTML = `<img src="img/${cid}.svg"> ${category.name}`;
		domCategoryFirst.classList.add("km-about-theme-entry");
		domLegendModalThemes.appendChild(domCategoryFirst);

		if(category.themes) {
			Object.entries(category.themes).forEach(e2 => {
				const [ tid, theme ] = e2;
				const domCatLineTheme = document.createElement("div");
				domCatLineTheme.classList.add("km-about-theme-entry");
				domCatLineTheme.innerHTML = `<img src="img/${tid}.svg"> ${theme.theme_name}`;
				domLegendModalThemes.appendChild(domCatLineTheme);
			});
		}
	});
}

// Event handler for theme click on switcher
function onThemeClick(e) {
	const target = e.target.nodeName.toLowerCase() === "img" ? e.target.parentNode : e.target;
	changeTheme(target.getAttribute("data-theme"));
}

// Show themes for current category in switcher
function showThemes(catId) {
	domIconDockThemes.innerHTML = '';

	if(catId === "all") {
		domIconDockThemes.classList.add("d-none");
	}
	else {
		domIconDockThemes.classList.remove("d-none");
		Object.entries(data.themesCategories[catId].themes).forEach((e, i) => {
			const [ tid, theme ] = e;

			const domTheme = document.createElement("a");
			domTheme.setAttribute("data-theme", tid);
			domTheme.setAttribute("href", "#");
			domTheme.title = theme.theme_name;

			const domThemeImg = document.createElement("img");
			domThemeImg.src = `img/${tid}.svg`;
			domThemeImg.title = theme.theme_name;
			domTheme.appendChild(domThemeImg);

			if(currentTheme === tid) {
				domTheme.classList.add("icon-dock-active");
			}

			domTheme.addEventListener("click", onThemeClick);
			domIconDockThemes.appendChild(domTheme);
		});
	}
}


/*
 * Data display
 */

// Change current category
function changeCategory(categoryId) {
	if(categoryId === currentCategory) { return null; }
	currentCategory = categoryId;

	for(let i=0; i < domIconDockCat.children.length; i++) {
		const c = domIconDockCat.children[i];
		if(c.nodeName.toLowerCase() === "a") {
			if(c.getAttribute("data-category") === currentCategory) {
				c.classList.add("icon-dock-active");
			}
			else {
				c.classList.remove("icon-dock-active");
			}
		}
	}

	showThemes(currentCategory);
}

// Change current theme
function changeTheme(themeId) {
	currentTheme = themeId;

	if(themeId !== null && currentCategory !== data.themes[themeId].category) {
		changeCategory(data.themes[themeId].category);
	}
	else {
		for(let i=0; i < domIconDockThemes.children.length; i++) {
			const t = domIconDockThemes.children[i];
			if(t.nodeName.toLowerCase() === "a") {
				if(t.getAttribute("data-theme") === currentTheme) {
					t.classList.add("icon-dock-active");
				}
				else {
					t.classList.remove("icon-dock-active");
				}
			}
		}
	}

	// Reload map layers
	updateMapLayers();

	// Change URL
	map.removeHash();
	const newurl = utils.setUrlParameter(window.location.href, "t", themeId === null ? currentCategory : themeId);
	window.history.replaceState({}, null, newurl);
	map.addHash();
}

// Reload map layers after events
function updateMapLayers() {
	if(geojsonLayer && geojsonLayer.options && geojsonLayer.options.style) {
		geojsonLayer.setStyle(geojsonLayer.options.style);
	}

	// Remove previous details layers
	if(tmsLayer && map.hasLayer(tmsLayer)) {
		map.removeLayer(tmsLayer);
		tmsLayer = null;
	}
	if(featuresLayer && map.hasLayer(featuresLayer)) {
		map.removeLayer(featuresLayer);
		featuresLayer = null;
	}

	// Add details layers
	if(data.themes && data.themes[currentTheme] && data.themes[currentTheme].theme_details) {
		if(data.themes[currentTheme].theme_details.tms) {
			tmsLayer = L.tileLayer(data.themes[currentTheme].theme_details.tms.url, Object.assign({ minZoom: ZOOM_DETAILS }, data.themes[currentTheme].theme_details.tms)).addTo(map);
		}
		else if(data.themes[currentTheme].theme_details.api) {
			lastType = null;
			afterZoom();
		}
	}

	// Handle details layers for categories
	if(!currentTheme && currentCategory !== "all") {
		lastType = null;
		afterZoom();
	}
}

//Function to add bottom menu or popup on each city
function processGeoJSONFeature(feature, layer) {
	layer.on("click", () => {
		const p = feature.properties;
		const missing = Object.entries(data.themes)
			.filter(e => {
				const [ tid, t ] = e;
				const diffvar = p["diff_"+tid];
				const avgvar = Math.min(data.statistics[p.cat_pop]["avg_"+tid], 0.5);
				return diffvar === null || diffvar <= avgvar * 4 / 5;
			})
			.sort((a, b) => p["diff_"+a[0]] - p["diff_"+b[0]])
			.map(e => e[0]);

		showCurrentCity(Object.assign({}, p, { missing: missing }));
	});
}

// Function to create symbol for a city (used when creating GeoJSON layer)
function pointFeature(feature, latlng) {
	const layer = L.circleMarker(latlng);
	layer._feature = feature;
	return layer;
}

// Display details features
function pointFeatureMinimal(feature, latlng) {
	return L.circleMarker(latlng, STYLE_FEATURE_POINTS);
}

// Function to get style (mainly color) for a given city symbol (used when creating GeoJSON layer)
function stylePoint(feature) {
	const p = feature.properties;
	const t = currentTheme || currentCategory;
	// Color
	const diffvar = p["diff_"+t];
	const avg = Math.min(data.statistics[p.cat_pop]["avg_"+t], 0.5);

	let color = null; // null = no data;
	if(diffvar === null) { color = LEGEND.neg2.color; } // Darkred
	else if(diffvar <= avg * 2 / 5) { color = LEGEND.neg2.color; } // Red
	else if(diffvar <= avg * 4 / 5) { color = LEGEND.neg1.color; } // Orange
	else if(diffvar <= avg * 6 / 5) { color = LEGEND.avg.color; } // Gray
	else if(diffvar <= avg * 8 / 5) { color = LEGEND.pos1.color; } // Lightgreen
	else { color = LEGEND.pos2.color; } // Green

	return {
		stroke: false,
		fillOpacity: 0.8,
		radius: MARKER_SIZES[p.cat_pop],
		fillColor: color
	};
}


/*
 * Data load after map movement
 */

function afterZoom() {
	const bounds = map.getBounds();
	const zoom = map.getZoom();
	let type = null;
	if(zoom >= ZOOM_DETAILS && currentTheme && data.themes && data.themes[currentTheme] && data.themes[currentTheme].theme_details.api) { type = "feature"; }
	else if(zoom >= ZOOM_DETAILS && currentCategory && !currentTheme) { type = "category"; }
	else if(zoom >= ZOOM_CITIES && zoom < ZOOM_DETAILS) { type = "stats"; }

	if(type !== "stats") {
		domLegend.classList.add("km-r-mobile");
	}
	else {
		domLegend.classList.remove("km-r-mobile");
	}

	if(!isDl) {
		if(zoom >= ZOOM_CITIES) {
			if(type && (!lastBounds || !lastBounds.contains(bounds) || !lastType || lastType !== type)) {
				if(timerDl) {
					clearTimeout(timerDl);
				}
				timerDl = setTimeout(() => {
					if(!isDl) {
						isDl = true;

						domDock.classList.remove("d-none");
						domDockLoader.classList.remove("d-none");
						domDockZoomin.classList.add("d-none");

						lastBounds = bounds;
						lastType = type;

						let myUrl = `${url}data/`;
						if(type === "feature") { myUrl += `pois/${currentTheme}`; }
						else if(type === "category") { myUrl += `pois/${currentCategory}`; }
						else { myUrl += "stats"; }
						myUrl += "?bbox="+bounds.toBBoxString();

						utils.request(myUrl)
						.then(txt => JSON.parse(txt))
						.then(geojson => {
							isDl = false;
							if(geojsonLayer && map.hasLayer(geojsonLayer)) {
								map.removeLayer(geojsonLayer);
							}
							if(featuresLayer && map.hasLayer(featuresLayer)) {
								map.removeLayer(featuresLayer);
							}

							if(type === "stats") {
								data.statistics = geojson.statistics;
								geojsonLayer = L.geoJSON(geojson, { pointToLayer: pointFeature, style: stylePoint, onEachFeature: processGeoJSONFeature });
								geojsonLayer.addTo(map);
							}
							else if(type === "feature" || type === "category") {
								featuresLayer = L.geoJSON(geojson, { pointToLayer: pointFeatureMinimal });
								featuresLayer.addTo(map);
							}

							domDockLoader.classList.add("d-none");
							domDockZoomin.classList.add("d-none");
							domDock.classList.add("d-none");
						})
						.catch(e => {
							isDl = false;
							domDockLoader.classList.add("d-none");
							domDockZoomin.classList.add("d-none");
							domDock.classList.add("d-none");
							console.error(e);
							alert("Erreur lors du téléchargement des données. Merci de réessayer plus tard.");
						});
					}
				}, 100);
			}
			else if(!type) {
				if(geojsonLayer && map.hasLayer(geojsonLayer)) {
					map.removeLayer(geojsonLayer);
				}
				if(featuresLayer && map.hasLayer(featuresLayer)) {
					map.removeLayer(featuresLayer);
				}
			}
		}
		else {
			domDock.classList.remove("d-none");
			domDockLoader.classList.add("d-none");
			domDockZoomin.classList.remove("d-none");
		}
	}
}

map.on("zoomend moveend", afterZoom);
map.on("click", e => {
	if(!lastCloseBottomMenuClick || lastCloseBottomMenuClick + 500 < Date.now()) {
		utils.request(`${url}whereami?lat=${e.latlng.lat}&lng=${e.latlng.lng}`)
		.then(res => showCurrentCity(JSON.parse(res)))
		.catch(e => console.error(e));
	}
});
