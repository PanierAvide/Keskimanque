// Array.flat polyfill
if (!Array.prototype.flat) {
	Object.defineProperty(Array.prototype, 'flat', {
		configurable: true,
		value: function flat () {
			var depth = isNaN(arguments[0]) ? 1 : Number(arguments[0]);

			return depth ? Array.prototype.reduce.call(this, function (acc, cur) {
				if (Array.isArray(cur)) {
					acc.push.apply(acc, flat.call(cur, depth - 1));
				} else {
					acc.push(cur);
				}

				return acc;
			}, []) : Array.prototype.slice.call(this);
		},
		writable: true
	});
}

/**
 * HTTP Get request
 */
exports.request = (url) => {
	return new Promise((resolve, reject) => {
		var xhr = new XMLHttpRequest();
		xhr.addEventListener("load", () => {
			resolve(xhr.responseText);
		});
		xhr.addEventListener("error", reject);
		xhr.addEventListener("abort", reject);
		xhr.open("GET", url);
		xhr.send();
	});
};

/**
 * Get an URL parameter
 */
exports.getUrlParameter = (url, parameter) => {
	parameter = parameter.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	var regex = new RegExp('[\\?|&]' + parameter.toLowerCase() + '=([^&#]*)');
	var results = regex.exec('?' + url.toLowerCase().split('?')[1]);
	return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

/**
 * Change an URL parameter
 */
exports.setUrlParameter = (url, key, value) => {
	var key = encodeURIComponent(key),
		value = encodeURIComponent(value);

	var baseUrl = url.split('#')[0].split('?')[0],
		newParam = key + '=' + value,
		params = '?' + newParam;

	let urlQueryString = null;
	if (url.split('?')[1] === undefined){ // if there are no query strings, make urlQueryString empty
		urlQueryString = '';
	} else {
		urlQueryString = '?' + url.split('?')[1].split('#')[0];
	}

	let hashString = url.includes('#') ? '#' + url.split('#')[1] : '';
	if(hashString === '#') { hashString = ''; }

	// If the "search" string exists, then build params from it
	if (urlQueryString) {
		var updateRegex = new RegExp('([\?&])' + key + '[^&#]*');
		var removeRegex = new RegExp('([\?&])' + key + '=[^&;#]+[&;#]?');

		if (value === undefined || value === null || value === '') { // Remove param if value is empty
			params = urlQueryString.replace(removeRegex, "$1");
			params = params.replace(/[&;]$/, "");

		} else if (urlQueryString.match(updateRegex) !== null) { // If param exists already, update it
			params = urlQueryString.replace(updateRegex, "$1" + newParam);

		} else if (urlQueryString == '') { // If there are no query strings
			params = '?' + newParam;
		} else { // Otherwise, add it to end of query string
			params = urlQueryString + '&' + newParam;
		}
	}

	// no parameter was set so we don't need the question mark
	params = params === '?' ? '' : params;

	return baseUrl + params + hashString;
};
