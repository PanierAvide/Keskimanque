#!/bin/bash

##############################################
#                                            #
# Download and import raw OpenStreetMap data #
#                                            #
##############################################

# Start date (for logs)
date

# Read configuration from JSON file
echo "======= Preparing ======="
for s in $(cat "../config.json" | jq -r "to_entries|map(\"\(.key)=\(.value|tostring)\")|.[]"); do
    export $s
done

start=`date +%s`

# DB to use (by default prod)
if [ "$1" == "next" ]; then
	usedb="${PG_DB_TMP}"
else
	usedb="${PG_DB_CLEAN}"
fi
echo "Will run on database ${usedb}"

# Process each theme
function type_to_sql {
	if [ "$first" = "0" ]; then
		echo "  UNION"
	fi

	if [ "$geom" = "point" ]; then
		echo "  SELECT way AS geom"
	else
		echo "  SELECT ST_Centroid(way) AS geom"
	fi

	echo "  FROM planet_osm_$geom"
	echo -n "  WHERE "
	basefct="to_entries|map(\"\\\"\(.key)\\\"='\(.value|tostring)'\")|join(\" AND \")"
	echo "${theme_where}" | jq -r "if type == \"object\" then $basefct else map($basefct|\"(\"+.+\")\")|join(\" OR \") end" | sed -r "s/='\*'/ IS NOT NULL/g;s/='([^']+\|[^']+)'/ IN ('\1')/g;s/\|/', '/g"
}

for theme_file in `ls ../themes/**/*.json`; do
	theme=$(basename -- "${theme_file}")
	theme=${theme/.json/}

	# Read parameters
	for s in $(cat "${theme_file}" | jq -r "to_entries|map(\"\(.key)=\(.value|tostring)\")|.[]" | sed -r 's/ /_/g'); do
		export $s
	done

	sql="
DROP VIEW IF EXISTS theme_${theme};
CREATE VIEW theme_${theme} AS
WITH features AS (
`first="1" ; echo "${theme_geomtypes}" | jq -r ".[]" | while read geom; do type_to_sql; first="0"; done`
),
nbf_zone AS (
  SELECT
    z.gid, z.area, z.cat_pop,
    COUNT(f.geom)::float / z.area AS density_features
  FROM zones z
  JOIN features f ON ST_Contains(z.geom, f.geom)
  GROUP BY z.gid
),
stats_zone AS (
  SELECT
    cat_pop,
    MAX(density_features) AS density_max
  FROM nbf_zone
  GROUP BY cat_pop
)
SELECT
  c.gid,
  density_features,
  ((cume_dist() OVER w) + (density_features / density_max)) / 2 AS pct_density
FROM nbf_zone c
LEFT JOIN stats_zone a ON c.cat_pop = a.cat_pop
WINDOW w AS (PARTITION BY c.cat_pop ORDER BY density_features);"

	echo "$sql" | psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -d "${usedb}"
done

# Create merge table
themes=$(for theme_file in $(ls ../themes/**/*.json); do theme=$(basename -- "${theme_file}"); echo "${theme/.json/}"; done)

sql="
DROP TABLE IF EXISTS zones_data;
CREATE TABLE zones_data AS
SELECT
  z.gid, z.name, z.population, z.cat_pop,
`i=1; for theme in $themes; do echo "  t$i.pct_density AS diff_$theme,"; i=$((i+1)); done`
  ST_Centroid(z.geom) AS geom
FROM zones z
`i=1; for theme in $themes; do echo "LEFT JOIN theme_$theme t$i ON t$i.gid = z.gid"; i=$((i+1)); done`;
CREATE INDEX zones_data_gid_idx ON zones_data(gid);
CREATE INDEX zones_data_geom_idx ON zones_data(geom);
`for theme in $themes; do echo "DROP VIEW theme_$theme;"; done`
DROP TABLE IF EXISTS zones_stats;
CREATE TABLE zones_stats AS
SELECT
`for theme in $themes; do echo "AVG(diff_$theme) AS avg_$theme,"; done`
  cat_pop
FROM zones_data
GROUP BY cat_pop;"

echo "$sql" | psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -d "${usedb}"


if [ "$1" == "next" ]; then
	# Alert API that database will be unavailable
	echo "Stop API queries to update database"
	lock_file="${WORK_DIR}/db.lock"
	echo "lock" > "${lock_file}"
	sleep 5

	# Rename databases to make next goes live
	echo "======= Switch next with current DB ======="
	psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid() AND datname='${PG_DB_CLEAN}'"
	set +e
	psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -d "${PG_DB_TMP}" -c "ALTER DATABASE ${PG_DB_CLEAN} RENAME TO ${PG_DB_OLD}"
	set -e
	psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -c "ALTER DATABASE ${PG_DB_TMP} RENAME TO ${PG_DB_CLEAN}"
	rm "${lock_file}"

	# Cleanup
	psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -c "DROP DATABASE IF EXISTS ${PG_DB_OLD}"
	psql -h "${PG_HOST}" -U "${PG_USER}" -p "${PG_PORT}" -d "${PG_DB_CLEAN}" -c "VACUUM FULL"
fi

end=`date +%s`

date
echo "Done in $((end-start)) seconds"
