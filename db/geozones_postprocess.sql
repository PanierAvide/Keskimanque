--
-- Post-processing of Geozones table
--

DELETE FROM zones WHERE level != 'fr:commune';
DELETE FROM zones WHERE successors IS NOT NULL;
REINDEX TABLE zones;

ALTER TABLE zones ADD COLUMN cat_pop VARCHAR;
UPDATE zones SET cat_pop='mt250000' WHERE level = 'fr:commune' AND population >= 250000;
UPDATE zones SET cat_pop='mt100000' WHERE level = 'fr:commune' AND cat_pop IS NULL AND population >= 100000;
UPDATE zones SET cat_pop='mt50000' WHERE level = 'fr:commune' AND cat_pop IS NULL AND population >= 50000;
UPDATE zones SET cat_pop='mt10000' WHERE level = 'fr:commune' AND cat_pop IS NULL AND population >= 10000;
UPDATE zones SET cat_pop='mt5000' WHERE level = 'fr:commune' AND cat_pop IS NULL AND population >= 5000;
UPDATE zones SET cat_pop='mt1000' WHERE level = 'fr:commune' AND cat_pop IS NULL AND population >= 1000;
UPDATE zones SET cat_pop='mt500' WHERE level = 'fr:commune' AND cat_pop IS NULL AND population >= 500;
UPDATE zones SET cat_pop='mt0' WHERE level = 'fr:commune' AND cat_pop IS NULL;
CREATE INDEX zones_cat_pop_idx ON zones(cat_pop);
